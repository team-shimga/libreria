<div class="modal" id="register">
        <div class="modal-content container">
            <span class="closebtn2 modal-close">&times;</span>
            <h5 class="center-align" style="margin-bottom:1%">REGISTRATION</h5>
            <form>
                <div class="input-field">
                    <input type="text" id="nome">
                    <label for="nome"  id="label-nome" >Your Name</label>
                </div>

                <div class="input-field">
                    <input type="text" id="cognome">
                    <label for="cognome"  id="label-cognome">Your Surname</label>
                </div>


                <div class="input-field">
                    <input type="text" id="indirizzo">
                    <label for="indirizzo"  id="label-indirizzo">Your Address</label>
                </div>

                <div class="input-field">
                    <input type="text" id="CAP">
                    <label for="CAP"  id="label-CAP">Your CAP</label>
                </div>

                <div class="input-field">
                    <input type="text" id="citta">
                    <label for="citta"  id="label-citta">Your City</label>
                </div>
                <div class="input-field">
                    <input type="text" id="paese">
                    <label for="paese"  id="label-paese">Your Conutry</label>
                </div>

                <div class="input-field">
                    <input type="tel" id="tel">
                    <label for="tel"  id="label-tel">Your Telephone number</label>
                </div>
                <div class="input-field">
                    <input type="email" id="email">
                    <label for="email" id="label-email">Your E-mail</label>
                </div>

                <div class="input-field">
                    <input type="password" id="pwd">
                    <label for="pwd" id="label-pwd">Your Password</label>
                </div>
                <div class="input-field">
                    <input type="password" id="pwd-repeat">
                    <label for="pwd-repeat" id="label-pwd-repeat">Repeat Your Password</label>
                </div>
                <div class="input-field">
                    <input type="text" id="cc">
                    <label for="cc" id="label-cc">Your Credit card number</label>
                </div>
                <div class="input-field">
                    <input type="text" id="cc-tipo">
                    <label for="cc-tipo" id="label-cc-tipo">Your Credit card type</label>
                </div>
                <div class="input-field">
                    <p id="par-scad">Exp. date</p>
                    <input type="month" id="scadenza" min="2020-01" max="2100-01" value="2020-01">
                </div>
                <div class="input-field">
                    <input type="number" id="cvv">
                    <label for="cvv" id="label-cvv">CVV</label>
                </div>

                <div class="input-field center">
                    <button type="submit" style="width: 100%;" class="btn-small indigo accent-2 white-text waves-effect waves-light" id="submit-register" >Registrati</button>
                </div>  
            </form>
        </div>
</div>

<div class="modal" id="login">
    <div class="modal-content container">
        <span class="closebtn2 modal-close">&times;</span>
        <h5 style="padding-bottom: 3%; text-align: center">LOGIN</h5>
        <form>
            <div class="input-field">
                <input type="email" id="email-login">
                <label for="email-login" id="label-email-login">Your E-mail</label>
            </div>
            <div class="input-field">
                <input type="password" id="pwd-login">
                <label for="pwd-login" id="label-pwd-login">Your Password</label>
            </div>

            <div class="input-field center">
                <button type="submit" style="width: 100%;" class="btn-small indigo accent-2 white-text waves-effect waves-light" id="submit-login">Login</button>
            </div>
        </form>  
    </div>
</div>