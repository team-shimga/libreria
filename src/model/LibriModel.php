<?php

namespace src\model;

use src\entity\Libro;

class LibriModel extends AbstractModel
{
    /**
     * @var string $tableName
     */
    protected static $tableName = 'libri';

    /**
     * @var string $entityClass
     */
    protected static $entityClass = Libro::class;

    /**
     * @var string  $primaryKey
     */
    protected static $primaryKey = 'id_libro';

    /**
     * LibriModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
