-- SELECT * FROM ordini, dettagli_ordine WHERE ordini.id_utente=1 AND dettagli_ordine.id_ordine= ordini.id_ordine AND NOT dettagli_ordine.stato='consegnato' 
-- SELECT *,
--         (SELECT COUNT(*)
--         FROM libreria.libri
--         WHERE id_reparto=1 AND sconto=20)
--         AS 'Numero libri "da non perdere"'
-- FROM libreria.libri
-- WHERE id_reparto=1 AND sconto=20

SELECT SUM(quantita), id_libro
FROM libreria.dettagli_ordine
GROUP BY id_libro
ORDER BY SUM(quantita) DESC
LIMIT 5;